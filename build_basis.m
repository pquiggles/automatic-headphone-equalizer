% build_basis(s) Builds the "basis vectors" of whatever headphone is
% currently on the microphone. It does this by setting the value of all
% digital pots to 255 (all 0 gain), except one, and obtaining the transfer
% function of the headphones with that band removed. It does this for all 6
% channels, and then builds a "basis" matrix A that will be used for the
% least squares fitting step.
% This function communicates with an Arduino currently connected on Serial
% object s.
%
% Jack Zhu
% Paul Quigley
% 2014

function A = build_basis(s)

figure();
basis_vectors = {};
offsets = [];
for i = 1:6
    gains = ones(6,1)*255;
    gains(i) = 0;
    writePots(s,gains,.5);
    
    fprintf(s,'analogPrint:begin');
    [freqs,  ampls, offset] = networkAnalyze_chirp(s);
    fprintf(s,'analogPrint:end');
    
    basis_vectors{i} = ampls;
    offsets(i) = offset;
    
    pause(.5);
    subplot(6,1,i);
    semilogx(freqs,ampls);
    drawnow;
end
drawnow;

%% Fix basis vectors
smallest = length(basis_vectors{1});
for i = 2:6
    smallest = min([smallest length(basis_vectors{i})]);
end

A = [];
for i = 1:6
    temp = basis_vectors{i};
    temp = temp (1:smallest);
    A(:,i) = temp' - offsets(i);
end

figure; semilogx(A);

end