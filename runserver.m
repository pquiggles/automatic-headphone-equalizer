% The main server loop of the MATLAB server. 

function runserver
close all
addpath(genpath('strings'));
delete(instrfind({'Port'},{'COM8'}));
s = serial('COM8');
fopen(s);

options = {'bass','treb','senn','beat'};

settings{1} = [0,0,0,255,255,255];
settings{2} = [255,255,255,0,0,0];
loaded = load('senn');
settings{3} = loaded.ampls;%[100,100,100,100,100,100];
loaded = load('beat');
settings{4} = loaded.ampls;%[100,100,100,100,100,100];

% Current Transfer Fn
current = [];
% Calibration Matrix
A = [];

senddelay = .5;

running = true;
    while(running == true)
        if(s.BytesAvailable <= 0)
            continue;
        end
        
        command = strtrim(fscanf(s))
        C = strsplit(command,':')
        if(strcmp(C{1},'beginChirp'))
            disp('Start Building Basis');
            
            % First gather the transfer function with everything set to 0
            gains = ones(6,1)*255;
            writePots(s,gains,senddelay);

            fprintf(s,'analogPrint:begin');
            [freqs,  ampls, offset] = networkAnalyze_chirp(s);
            fprintf(s,'analogPrint:end');
            current = ampls

            % Then build the basis vectors            
            A = build_basis(s);
            save('last','A');

        elseif(strcmp(C{1},'selectOption'))
            ind = find(ismember(options,C{2}));
            disp(['selectOption to ' options{ind}]);
            if(length(settings{ind})==6)
                disp('Size 6 found, using setting as gain');
                writePots(s,settings{ind},senddelay);
            else
                disp('Larger size, performing Least Squares Estimate');
                target_fit(s,A,settings{ind});
            end
        end
        
        drawnow;
    end
end