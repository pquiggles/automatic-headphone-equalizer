% networkAnalyze_chirp(serialobj) runs a frequency sweep, in the form of a
% lograithmic chirp function, and plays the resulting sound from the
% computer speakers. It then grabs the value of the peak detector (being
% broadcast by the Arduino), and returns the amplitudes and
% the offset of the response.

function [frequencies, amplitudes, offset] = networkAnalyze_chirp(serialobj)
    % Set sweep parameters
    freqstart = 20; % Start freqency, in Hz
    freqend = 15000; % End frequency, in Hz
    Fs = 44.1e3; % Sampling rate
    soundlength = 10; % Sound length, in seconds
    
    t = linspace(0,soundlength,Fs*soundlength);
    y = chirp(t,freqstart,t(end),freqend,'logarithmic');
    A = audioplayer(y,Fs);
    
    
    disp('Measuring Offset');
    % Pre-measure offset
    total = 0;
    count = 0;
    while(count < 200)
        if(serialobj.BytesAvailable > 0)
            c = str2double(fscanf(serialobj));
            if(length(c) == 1 && ~isnan(c))
                total = total + c;
                count = count + 1;
            end
        end
    end
    offset = total/200;
    
    disp('Running Sound Loop');
    % Begin sound loop
    play(A);
    
    amplitudes = [];
    while(isplaying(A))
        if(serialobj.BytesAvailable > 0)
            c = str2double(fscanf(serialobj));
            if(length(c) == 1 && ~isnan(c))
                amplitudes(end+1) = c;
            end
        end
    end
    
    frequencies = logspace(log10(freqstart),log10(freqend),length(amplitudes));
end