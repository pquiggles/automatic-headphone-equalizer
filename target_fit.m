% target_fit(s, A, target) Attempts to fit the current headphones basis
% vector matrix "A", into the target headphone's transfer function via
% least squares

function target_fit(s,A, target)
%% Make them the same length
target = interp1(1:length(target),target,linspace(1,length(target),size(A,1)),'pchip');

%% Attempt to fit a flat band
b = target;
x = A\b;
%Must clip to 0
x(x<0) = 0;
x(x>1) = 1;
figure;
disp('Vals: ');
disp(x);
plot(A*x); title('A*x');

%% Check out the error
targetgains = floor(255*(1-x));
writePots(s,targetgains,.5);

fprintf(s,'analogPrint:begin');
[freqs,  ampls, offset] = networkAnalyze_chirp(s);
fprintf(s,'analogPrint:end');figure;
plot(ampls); title('Measured adjusted Response');

end