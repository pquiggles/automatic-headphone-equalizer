% writePots(s,chosen,delay) publishes the values of chosen (an array of
% values, length 6, values 0 - 255) to the Arduino. it pauses between each
% send by an amount specified by delay in order to ensure the Arduino is
% finished writing to SPI. (We are very conservative with the delay, and it
% is very possible this is not needed).
function writePots(s,chosen,delay)
    for i = 1:length(chosen)
        potcommand = sprintf('potWrite:%d,%d\n',i-1,chosen(i))
        fprintf(s,potcommand,'sync');
        disp(['Sent ' num2str(chosen(i)) ' to pot ' num2str(i)]);
        pause(delay);
    end
end